package com.jb1services.chaosutil.util.ui;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.jb1services.chaosutil.util.helpers.D2IntPoint;

class TestIOUtil {

	@Test
	public void testFindOccurences() 
	{
		Path tfile = Paths.get("test-resources/find_occurences");
		assertTrue(tfile.toFile().exists());
		
		Map<String, List<D2IntPoint>> occurencesMap = IOUtil.findOccurences(tfile, "abc");
		assertEquals(occurencesMap.size(), 1);
		assertEquals(occurencesMap.get("abc").size(), 4);
		
		occurencesMap = IOUtil.findOccurences(tfile, "((?<=\\s)|^)abc((?=\\s)|$)", true);
		assertEquals(occurencesMap.size(), 1);
		assertEquals(occurencesMap.get("abc").size(), 3);
	}
}
