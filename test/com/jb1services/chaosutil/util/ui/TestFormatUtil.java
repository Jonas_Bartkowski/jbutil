package com.jb1services.chaosutil.util.ui;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.jb1services.chaosutil.util.formating.FormatUtil;

public class TestFormatUtil
{
	@Test
	public void formatLongDuration_millis()
	{
		int years = 3;
		int months = 2;
		int days = 4;
		int hours = 14;
		int minutes = 30;
		int seconds = 60;
		int millis = 4;
		long millis_total = millis + 1000 * seconds + 1000 * 60 * minutes + 1000 * 60 * 60 * hours
				+ 1000 * 60 * 60 * 24 * days + 1000 * 60 * 20 * 24 * 30 * months
				+ 1000 * 60 * 60 * 24 * 30 * 365 * years;
		assertEquals(FormatUtil.formatLongTime(millis_total),
				"2 weeks 2 days 2 hours 47 minutes 57 seconds 1392477348 milliseconds.");
	}
}
