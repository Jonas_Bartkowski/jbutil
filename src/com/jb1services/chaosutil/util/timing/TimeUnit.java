package com.jb1services.chaosutil.util.timing;

import java.util.AbstractMap.SimpleEntry;

public enum TimeUnit
{
	SECOND(1000), MINUTE(SECOND.millis * 60), HOUR(MINUTE.millis * 60), DAY(HOUR.millis * 24), WEEK(DAY.millis * 7),
	THIRTY_DAY_MONTH(DAY.millis * 30), THIRTY_ONE_DAY_MONTH(DAY.millis * 31), FEBUARY(DAY.millis * 28), LEAP_JEAR_FEBRUARY(DAY.millis * 29),
	QUARTER_1(DAY.millis * 90), QUARTER_1_LEAP_YEAR(DAY.millis * 91), QUARTER_2(DAY.millis * 91), QUARTER_3_4(DAY.millis * 92),
	NORMAL_YEAR(DAY.millis * 365), LEAP_YEAR(DAY.millis * 366);
	
	private long millis;
	
	TimeUnit(long millis)
	{
		this.millis = millis;
	}
	
	public long getMillis()
	{
		return millis;
	}
	
	public static SimpleEntry<Integer, TimeUnit> getMultiple(Integer howMuch, TimeUnit tu)
	{
		return new SimpleEntry<Integer, TimeUnit>(howMuch, tu);
	}
	
	public String toString()
	{
		return "milliseconds: "+millis;
	}
}