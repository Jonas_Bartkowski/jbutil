package com.jb1services.chaosutil.util.timing;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Duration
{
	HashMap<TimeUnit, Integer> timeUnity = new HashMap<>();
	
	public Duration(List<SimpleEntry<TimeUnit, Integer>> timeUnity)
	{
		for (SimpleEntry<TimeUnit, Integer> tu : timeUnity)
		{
			Integer amount = this.timeUnity.get(tu.getKey());			
			if (amount != null)
				this.timeUnity.put(tu.getKey(), amount + tu.getValue());
			else
				this.timeUnity.put(tu.getKey(), tu.getValue());
		}
	}
	
	public Duration(SimpleEntry<TimeUnit, Integer>... timeUnity)
	{
		this(Arrays.asList(timeUnity));
	}
	
	public long getTotalDuration()
	{
		long total = 0;
		
		for (TimeUnit key : timeUnity.keySet())
		{
			total += key.getMillis() * timeUnity.get(key).intValue();
		}
		
		return total;
	}
	
	
}
