package com.jb1services.chaosutil.util.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.Range;
import org.mozilla.universalchardet.UniversalDetector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jb1services.chaosutil.util.helpers.D2IntPoint;

public class IOUtil
{
    public static final String DEFAULT_QUIT_INPUT = "q";

    public static Scanner scan = new Scanner(System.in);

    public static String next(String prompt, String errorMsg, Predicate<String> predicate, String quitInput)
    {
        if (prompt != null)
            sout(prompt);
        if (quitInput != null)
        	sout("Enter '" + quitInput + "' to abort!");
        String uinput = scan.nextLine();
        if (quitInput != null && quitInput.equalsIgnoreCase(uinput))
        	return null;
        else if (predicate.test(uinput))
            return uinput;
        if (errorMsg != null)
        	serr(errorMsg);
        return next(prompt, errorMsg, predicate, quitInput);
    }

    public static String next(String prompt, String errorMsg, Predicate<String> predicate)
    {
    	return next(prompt, errorMsg, predicate, "q");
    }

    public static String next(String prompt, Predicate<String> predicate)
    {
    	return next(prompt, "Sorry, that's an invalid input!", predicate, "q");
    }

    public static String next_no_quit(String prompt, Predicate<String> predicate)
    {
    	return next(prompt, "Sorry, that's an invalid input!", predicate, null);
    }

    public static String next_no_quit(String prompt, String... validStrings)
    {
    	return next(prompt, "Sorry, that's an invalid input!", validStrings, null);
    }

    public static String next_no_quit(String prompt, String errorMsg, Predicate<String> predicate)
    {
    	return next(prompt, errorMsg, predicate, null);
    }

    public static String next_invalid(boolean ignoreCase, String prompt, String errorMsg, Collection<String> invalidInputs, String quitInput)
    {
        Predicate<String> pred = (str) -> {return invalidInputs.stream().filter(ii -> ignoreCase ? !ii.equalsIgnoreCase(str) : !ii.equals(str)).findAny().isPresent();};
        return next(prompt, errorMsg, pred, quitInput);
    }

    public static String next_invalid(boolean ignoreCase, String prompt, String errorMsg, Collection<String> invalidInputs)
    {
        Predicate<String> pred = (str) -> {return invalidInputs.stream().filter(ii -> ignoreCase ? !ii.equalsIgnoreCase(str) : !ii.equals(str)).findAny().isPresent();};
        return next(prompt, errorMsg, pred, DEFAULT_QUIT_INPUT);
    }

    public static String next(boolean ignoreCase, String prompt, String errorMsg, String quitInput, String[] validInputs)
    {
        Predicate<String> pred = (str) -> {return Arrays.asList(validInputs).stream().filter(ii -> ignoreCase ? ii.equalsIgnoreCase(str) : ii.equals(str)).findAny().isPresent();};
        return next(prompt, errorMsg, pred, quitInput);
    }

    public static String next(boolean ignoreCase, String prompt, String errorMsg, String... validInputs)
    {
        Predicate<String> pred = (str) -> {return Arrays.asList(validInputs).stream().filter(ii -> ignoreCase ? ii.equalsIgnoreCase(str) : ii.equals(str)).findAny().isPresent();};
        return next(prompt, errorMsg, pred, DEFAULT_QUIT_INPUT);
    }


    public static String next(boolean ignoreCase, String prompt, String errorMsg, Collection<String> validInputs, String quitInput)
    {
    	if (validInputs != null && !validInputs.isEmpty())
    	{
	        Predicate<String> pred = (str) -> {return validInputs.stream().filter(ii -> ii != null ? ignoreCase ? ii.equalsIgnoreCase(str) : ii.equals(str) : false).findAny().isPresent();};
	        return next(prompt, errorMsg, pred, quitInput);
    	}
        else throw new IllegalArgumentException("valid inputs can't be null or empty!");
    }

    public static String next(boolean ignoreCase, String prompt, String errorMsg, Collection<String> validInputs)
    {
    	return next(ignoreCase, prompt, errorMsg, validInputs, DEFAULT_QUIT_INPUT);
    }

    public static String next(boolean ignoreCase, String prompt, String errorMsg, String[] validInputs, String quitInput)
    {
    	return next(ignoreCase, prompt, errorMsg, Arrays.asList(validInputs), quitInput);
    }

    public static String next(String prompt, String errorMsg, String[] validInputs, String quitInput)
    {
    	return next(false, prompt, errorMsg, Arrays.asList(validInputs), quitInput);
    }

    public static String next(String prompt, String errorMsg, String... validInputs)
    {
        return next(false, prompt, errorMsg, Arrays.asList(validInputs));
    }

    public static String next_invalid(String prompt, String errorMsg, String... invalidInputs)
    {
        return next_invalid(false, prompt, errorMsg, Arrays.asList(invalidInputs));
    }

    public static String next(String prompt, String errorMsg, Collection<String> validInputs, String quitInput)
    {
        return next(false, prompt, errorMsg, validInputs, quitInput);
    }

    public static String next(String prompt, String errorMsg, Collection<String> validInputs)
    {
        return next(false, prompt, errorMsg, validInputs);
    }

    public static Integer next_index_valid(String prompt, List<String> validInputs)
    {
    	return next_index_valid(prompt, "Sorry, but that's not valid. valid inputs:\n" + validInputs.stream().map(str -> validInputs.indexOf(str) + ": " + str).collect(Collectors.joining("\n")), validInputs, false, "q");
    }

    public static Integer next_index_valid(String prompt, String errorMsg, List<String> validInputs, boolean ignoreCase, String quitInput)
    {
    	if (validInputs != null && !validInputs.isEmpty())
    	{
			Function<String, Integer> pred = str ->
			{
	    		try
	    		{
	    			Integer index = Integer.valueOf(str);
	    			return (index >= 0 && index < validInputs.size()) ? index : null;
	    		}
	    		catch (NumberFormatException e)
	    		{
	    			if (!ignoreCase)
	    				return validInputs.indexOf(str);
	    			else
	    			{
	    				for (int i = 0; i < validInputs.size(); i++)
	    				{
	    					if (validInputs.get(i).equalsIgnoreCase(str))
	    					{
	    						return i;
	    					}
	    				}
	    				return null;
	    			}
	    		}
			};

	    	if (prompt != null)
	            sout(prompt);
	        if (quitInput != null)
	        	sout("Enter '" + quitInput + "' to abort!");
	        String uinput = scan.nextLine();
	        if (quitInput != null && quitInput.equalsIgnoreCase(uinput))
	        	return null;
	        else if (pred.apply(uinput) != null)
	            return pred.apply(uinput);
	        if (errorMsg != null)
	        	serr(errorMsg);
	        return next_index_valid(prompt, errorMsg, validInputs, ignoreCase, quitInput);
    	}
    	else throw new IllegalArgumentException("valid inputs can't be null or empty!");
    }

    public static String next(String prompt, Collection<String> validInputs)
    {
        if (validInputs != null && !validInputs.isEmpty())
            return next(false, prompt, "Sorry, but that's not valid. valid inputs:\n" + validInputs.stream().collect(Collectors.joining("\n")), validInputs);
        else throw new IllegalArgumentException("valid inputs can't be null or empty!");
    }

    public static String next(String prompt, Collection<String> validInputs, String quitInput)
    {
        return next(false, prompt, "Sorry, but that's not valid. valid inputs:\n" + validInputs.stream().collect(Collectors.joining("\n")), validInputs, quitInput);
    }

//	public static String next(String prompt, String errorMsg, Set<String> validInputs)
//	{
//		return next(false, prompt, errorMsg, validInputs);
//	}

    public static String next_invalid(String prompt, String errorMsg, Collection<String> invalidInputs, String quitInput)
    {
        return next_invalid(false, prompt, errorMsg, invalidInputs, quitInput);
    }

    public static String next_invalid(String prompt, String errorMsg, Collection<String> invalidInputs)
    {
        return next_invalid(false, prompt, errorMsg, invalidInputs);
    }

    public static String next(boolean ignoreCase, String prompt, String errorMsg, Set<String> validInputs)
    {
        return next(ignoreCase, prompt, errorMsg, validInputs);
    }

    public static String next(boolean ignoreCase, String errorMsg, Set<String> validInputs,
            String... additionalPossibilities)
    {
        while (true)
        {
            String uinput = scan.nextLine();
            for (String vi : validInputs)
            {
                if (ignoreCase ? vi.equalsIgnoreCase(uinput) : vi.equals(uinput))
                {
                    return vi;
                }
            }
            for (String vi : additionalPossibilities)
            {
                if (ignoreCase ? vi.equalsIgnoreCase(uinput) : vi.equals(uinput))
                {
                    return vi;
                }
            }
            sout(errorMsg);
            return next(ignoreCase, errorMsg, validInputs);
        }
    }

    public static String next_non_empty(String prompt)
    {
    	return next(prompt, "You cannot enter an empty String!", (str) -> !str.trim().isEmpty(), "q");
    }

    public static String next_non_empty(String prompt, String errorMessage)
    {
    	return next(prompt, errorMessage, (str) -> !str.trim().isEmpty(), "q");
    }

    public static String next_non_empty(String prompt, String errorMessage, String quitInput)
    {
    	return next(prompt, errorMessage, (str) -> !str.trim().isEmpty(), quitInput);
    }

    public static String next(String... validInputs)
    {
    	return next(true, null, "Invalid input. Try again!", Arrays.asList(validInputs));
    }

    public static String next(String prompt, String... validInputs)
    {
        return next(prompt, "Invalid input. Try again!", validInputs);
    }

    public static boolean nextYesNo(String yesSign, String noSign)
    {
        String in = next(true, "(" + yesSign + "/" + noSign + ")", yesSign, noSign);
        return (in.equalsIgnoreCase(yesSign));
    }

    public static Boolean confirm(String confirmMessage, String quitInput)
    {
        sout(confirmMessage);
        sout("(y/n)" + (quitInput != null ? " (Enter " + quitInput + " to abort)" : ""));
        String[] validInputs = quitInput != null ? new String[] { "y", "n", quitInput } : new String[] { "y", "n"};
        String uin = next(validInputs);
        if (uin != null)
        {
	        if (uin.equals("y"))
	        {
	            return true;
	        } else if (uin.equals("n"))
	        {
	            return false;
	        }

	        if (quitInput != null)
	        {
	        	return uin.equals(quitInput) ? null : confirm(confirmMessage, quitInput);
	        }
	        else
	        {
	        	return confirm(confirmMessage, quitInput);
	        }
        }
        else
        	return null;
    }

    public static Boolean confirm(String confirmMessage)
    {
    	return confirm(confirmMessage, DEFAULT_QUIT_INPUT);
    }

    public static Integer nextInt(int... possibleValues)
    {
        return nextInt(null, null, possibleValues);
    }

    public static Integer nextInt(String prompt, int... possibleValues)
    {
        return nextInt(prompt, null, possibleValues);
    }

    public static Integer nextInt(String prompt, String errorMessage, int... possibleValues)
    {
        if (prompt != null)
            sout(prompt);

        String uin = scan.nextLine();
        if (!uin.equals(DEFAULT_QUIT_INPUT))
        {
	        try
	        {
	            int val = Integer.valueOf(uin);
	            if (possibleValues.length < 1 || Arrays.asList(possibleValues).contains(val))
	                return val;
	            if (errorMessage != null)
	                sout(errorMessage);
	            return nextInt(prompt, errorMessage, possibleValues);
	        } catch (NumberFormatException e)
	        {
	            if (errorMessage != null)
	                sout(errorMessage);
	            return null;
	        }
        }
        return null;
    }

    public static Integer nextInt(String message, Range<Integer> possibleRange)
    {
        return nextInt(message, possibleRange, "This was not in the range. Range is "+possibleRange);
    }

    public static Integer nextInt(String message, Range<Integer> possibleRange, String errorMessage, String quitInput)
    {
        Predicate<String> pred = (str) -> {try {Integer num = Integer.valueOf(str); return possibleRange.contains(num);} catch(NumberFormatException e) {return false;}};
        return Integer.valueOf(next(message, errorMessage, pred, quitInput));
    }

    public static Integer nextInt(String message, Range<Integer> possibleRange, String errorMessage)
    {
        Predicate<String> pred = (str) -> {try {Integer num = Integer.valueOf(str); return possibleRange.contains(num);} catch(NumberFormatException e) {return false;}};
        String res = next(message, errorMessage, pred, DEFAULT_QUIT_INPUT);
        if (res != null)
        	return Integer.valueOf(res);
        else
        	return null;
    }

    public static Integer nextInt_q(String message, Range<Integer> possibleRange, String quitInput)
    {
        Predicate<String> pred = (str) -> {try {Integer num = Integer.valueOf(str); return possibleRange.contains(num);} catch(NumberFormatException e) {return false;}};
        String val = (next(message, null, pred, quitInput));
        if (val != null)
        	return Integer.valueOf(val);
        else
        	return null;
    }

    public static Integer nextInt(Range<Integer> possibleRange)
    {
        return nextInt("Please select an integer between "+possibleRange.getMinimum() + " and "+possibleRange.getMaximum(), possibleRange, "Sorry, but that was not within the range.");
    }

    public static String nextLine()
    {
        return scan.nextLine();
    }

    public static String nextLine(String prompt)
    {
        sout(prompt);
        return scan.nextLine();
    }

    public static String nextLine(String prompt, String quitInput)
    {
        sout(prompt);
        String input = scan.nextLine();
        if (input.equalsIgnoreCase(quitInput))
        	return null;
        return input;
    }

    public static String returnInputMatchingOrNull(String regex)
    {
        return returnInputMatchingOrNull(regex, null, null);
    }

    public static String returnInputMatchingOrNull(String regex, String prompt)
    {
        return returnInputMatchingOrNull(regex, null, prompt);
    }

    public static String returnInputMatchingOrNull(String regex, Function<String, String> preManipulation)
    {
        return returnInputMatchingOrNull(regex, preManipulation, null);
    }

    public static String returnInputMatchingOrNull(String regex, Function<String, String> preManipulation,
            String prompt)
    {
        if (prompt != null)
            sout(prompt);
        String input = scan.nextLine();
        if (preManipulation != null)
            input = preManipulation.apply(input);
        return matches(input, regex);
    }

    public static String returnInputContainingOrNull(String regex)
    {
        return returnInputFindOrNull(regex, null, null);
    }

    public static String returnInputFindOrNull(String regex, Function<String, String> preManipulation)
    {
        return returnInputFindOrNull(regex, preManipulation, null);
    }

    public static String returnInputFindOrNull(String regex, Function<String, String> preManipulation,
            String preMessage)
    {
        if (preMessage != null)
            sout(preMessage);
        String input = scan.nextLine();
        if (preManipulation != null)
            input = preManipulation.apply(input);
        return find(input, regex);
    }

    public static void sout(Object out)
    {
        System.out.println(out);
    }

    public static void serr(Object out)
    {
        System.err.println(out);
    }

    public static String matches(String string, String regex)
    {
        Matcher match = Pattern.compile(regex).matcher(string);
        if (match.find())
            return string;
        else
            return null;
    }

    public static String find(String string, String regex)
    {
        Matcher match = Pattern.compile(regex).matcher(string);
        if (match.find())
            return string;
        else
            return null;
    }

    public static Date nextDate(String prompt, String errorPrompt, String quitCharacter, SimpleDateFormat sdf)
    {
        sout(prompt);
        sout("The requested format is " + sdf.toPattern() + ". Enter '" + quitCharacter + "' to quit.");
        Date date = null;
        String input = "";
        do
        {
            input = nextLine();
            try
            {
                date = sdf.parse(scan.nextLine());
            } catch (ParseException e)
            {
                sout(errorPrompt);
            }
        } while (date == null && !input.equals(quitCharacter));
        return date;
    }

    public static Date nextDate(String prompt, SimpleDateFormat sdf)
    {
        return nextDate(prompt,
                "Sorry, that was not correct; you need to enter the date in exactly this pattern: " + sdf.toPattern(),
                "q", sdf);
    }

    public static Date nextDate(String prompt)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        return nextDate(prompt,
                "Sorry, that was not correct; you need to enter the date in exactly this pattern: " + sdf.toPattern(),
                "q", sdf);
    }

    public static Date nextDate(SimpleDateFormat sdf)
    {
        sout("Please enter a date in this format: " + sdf.toPattern() + " or '" + DEFAULT_QUIT_INPUT + "' to quit!");
        Date date = null;
        String input = "";
        do
        {
            input = nextLine();
            try
            {
                date = sdf.parse(scan.nextLine());
            } catch (ParseException e)
            {
                sout("Sorry, that was not correct; you need to enter the date in exactly this pattern: "
                        + sdf.toPattern());
            }
        } while (date == null && !input.equals(DEFAULT_QUIT_INPUT));
        return date;
    }

    public static Date nextDate()
    {
        return nextDate(new SimpleDateFormat("mm/dd/yyyy"));
    }

    public static String readString(Path path) throws IOException
    {
    	return Files.readAllLines(path).stream().collect(Collectors.joining("\n"));
    }

    public static JsonElement readJSON(Path path) throws JsonSyntaxException, IOException
    {
    	JsonParser gson = new JsonParser();
    	return gson.parse(readString(path));
    }
    
    public static <T> T readJSONToObject(Path path, Type type) throws JsonSyntaxException, IOException
    {
    	Gson gson = new Gson();
    	return gson.fromJson(readString(path), type);
    }

    public static void writeObject(String path, Object object)
    {
    	writeObject(Paths.get(path), object);
    }

    public static void writeObject(Path path, Object object)
    {
        try
        {
            FileOutputStream fos = new FileOutputStream(path.toFile());
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void writeJSON(String path, JsonElement jsonElement)
    {
    	writeJSON(Paths.get(path), jsonElement);
    }

    public static void writeJSON(Path path, JsonElement jsonElement)
    {
    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
    	writeString(path, gson.toJson(jsonElement));
    }

    public static void writeJSON(String path, Object object)
    {
    	writeJSON(Paths.get(path), object);
    }

    public static void writeJSON(Path path, Object object)
    {
    	Gson gson = new GsonBuilder().setPrettyPrinting().create();
		writeString(path, gson.toJson(object));
    }

    public static void writeString(String path, String data)
    {
    	writeString(Paths.get(path), data);
    }

    public static void writeString(Path path, String data)
    {
    	try (PrintStream out = new PrintStream(new FileOutputStream(path.toFile())))
		{
			out.print(data);
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public static String readLinesToString(String path)
    {
    	return readLinesToString(Paths.get(path));
    }

    public static String readLinesToString(Path path)
    {
    	try
		{
			return Files.readAllLines(path).stream().collect(Collectors.joining("\n"));
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }

    public static <T> T readObject(String path)
    {
    	return readObject(Paths.get(path));
    }

    public static <T> T readObject(Path path)
    {
        try
        {
            FileInputStream fis = new FileInputStream(path.toFile());
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = ois.readObject();
            fis.close();
            ois.close();
            return (T) (obj);
        } catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        throw new IllegalStateException("Unknown state in readObject");
    }

    public static void replaceLines(String path, String... replacementPairs) throws IOException
    {
    	replaceLines(path, false, replacementPairs);
    }

    public static void replaceLines(String path, boolean regex, String... regexReplacementPairs) throws IOException
    {
    	replaceLines(Paths.get(path), regex, regexReplacementPairs);
    }

    public static void replaceLines(Path path, String... replacementPairs) throws IOException
    {
    	replaceLines(path, false, replacementPairs);
    }

    public static void replaceLines(Path path, boolean regex, String... replacementPairs) throws IOException
    {
    	List<String> lines = Files.readAllLines(path);
    	for (int ii = 0; ii+1 < replacementPairs.length; ii+=2)
    	{
    		final int i = ii;
    		if (regex)
    			lines = lines.stream().map(line -> line.replaceAll(replacementPairs[i], replacementPairs[i+1])).collect(Collectors.toList());
    		else
    			lines = lines.stream().map(line -> line.replace(replacementPairs[i], replacementPairs[i+1])).collect(Collectors.toList());
    	}
    	writeLines(path, lines);
    }

    public static void copyAndReplaceLines(String originPath, String targetPath, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originPath, targetPath, false, replacementPairs);
    }

    public static void copyAndReplaceLines(URL originURL, String targetPath, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originURL, targetPath, false, replacementPairs);
    }

    public static void copyAndReplaceLines(URL originURL, String targetPath, boolean regex, String... regexReplacementPairs) throws IOException
    {
    	copyAndReplaceLines(originURL, Paths.get(targetPath), regex, regexReplacementPairs);
    }

    public static void copyAndReplaceLines(String originPath, String targetPath, boolean regex, String... regexReplacementPairs) throws IOException
    {
    	copyAndReplaceLines(Paths.get(originPath), Paths.get(targetPath), regex, regexReplacementPairs);
    }

    public static void dudeldu()
    {

    }

    public static void copyAndReplaceLines(Path originPath, Path targetPath, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originPath, targetPath, false, replacementPairs);
    }

    public static void copy(String originPath, String targetPath) throws IOException
    {
    	copyTextFile(Paths.get(originPath), Paths.get(targetPath));
    }

    public static void copyTextFile(Path originPath, Path targetPath) throws IOException
    {
    	List<String> lines = Files.readAllLines(originPath);
    	writeLines(targetPath, lines);
    }

    public static void copyAndReplaceLines(Path originPath, Path targetPath, boolean regex, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originPath, targetPath, regex, false);
    }

    public static void copyAndReplaceLines(Path originPath, Path targetPath, boolean regex, boolean overwrite, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originPath, targetPath, regex, overwrite, Arrays.asList(replacementPairs));
    }

    public static void copyAndReplaceLines(URL originURL, Path targetPath, boolean regex, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originURL, targetPath, regex, false, Arrays.asList(replacementPairs));
    }

    public static void copyAndReplaceLines(URL originURL, Path targetPath, boolean regex, boolean overwrite, String... replacementPairs) throws IOException
    {
    	copyAndReplaceLines(originURL, targetPath, regex, overwrite, Arrays.asList(replacementPairs));
    }

    public static void copyAndReplaceLines(URL originURL, Path targetPath, boolean regex, boolean overwrite, List<String> replacementPairs) throws IOException
    {
    	BufferedReader reader = new BufferedReader(new InputStreamReader(originURL.openStream()));

    	List<String> lines = new ArrayList<>();
        String linee;
        while ((linee = reader.readLine()) != null)
        {
        	lines.add(linee);
        }
        reader.close();

    	for (int ii = 0; ii+1 < replacementPairs.size(); ii+=2)
    	{
    		final int i = ii;
    		if (regex)
    			lines = lines.stream().map(line -> line.replaceAll(replacementPairs.get(i), replacementPairs.get(i+1))).collect(Collectors.toList());
    		else
    			lines = lines.stream().map(line -> line.replace(replacementPairs.get(i), replacementPairs.get(i+1))).collect(Collectors.toList());
    	}
    	writeLines(targetPath, lines, overwrite);
    }

    public static void copyAndReplaceLines(Path originPath, Path targetPath, boolean regex, boolean overwrite, List<String> replacementPairs) throws IOException
    {
    	List<String> lines = Files.readAllLines(originPath);
    	for (int ii = 0; ii+1 < replacementPairs.size(); ii+=2)
    	{
    		final int i = ii;
    		if (regex)
    			lines = lines.stream().map(line -> line.replaceAll(replacementPairs.get(i), replacementPairs.get(i+1))).collect(Collectors.toList());
    		else
    			lines = lines.stream().map(line -> line.replace(replacementPairs.get(i), replacementPairs.get(i+1))).collect(Collectors.toList());
    	}
    	writeLines(targetPath, lines, overwrite);
    }

    /**
     * Like writeLines(Path path, List<String> lines, boolean overwrite),
     * but without overwriting.
     */
    public static void writeLines(Path path, List<String> lines) throws IOException, FileNotFoundException
    {
    	writeLines(path, lines, false);
    }

    /**
     * Like writeLines(Path path, List<String> lines, boolean overwrite, boolean buffered),
     * buf without buffering.
     */
    public static void writeLines(Path path, List<String> lines, boolean overwrite) throws IOException, FileNotFoundException
    {
    	writeLines(path, lines, overwrite, false);
    }

    /**
     * Writes the given @param lines to the given @param path.
     * @param overwrite: whether path should be completely overwritten.
     * @param buffered: if writing should be buffered.
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void writeLines(Path path, List<String> lines, boolean overwrite, boolean buffered) throws IOException, FileNotFoundException
    {
    	File file = path.toFile();
    	File parent = file.getParentFile();
    	if (parent.exists())
    	{
    		if (!parent.isDirectory())
    			throw new IllegalArgumentException("Parent file needs to be directory!");
    	}
    	else
    	{
    		Files.createDirectories(parent.toPath());
    	}

    	if (file.exists())
    	{
    		if (!overwrite)
    			throw new IllegalArgumentException("Target file exists & overwrite is set to false!");
    	}
    	else
    		file.createNewFile();

    	Writer writer = new FileWriter(path.toFile());
		if (buffered)
    		 writer = new BufferedWriter(writer);
    	for (String line : lines)
    	{
	    	writer.write(line + "\n");
    	}
    	writer.close();
    }

    /**
     * Like writeLines_append(Path path, List<String> lines, boolean buffered),
     * but without buffering.
     */
    public static void writeLines_append(Path path, List<String> lines) throws IOException, FileNotFoundException
    {
    	writeLines_append(path, lines, false);
    }

    /**
     * Write given @param lines to @param path.
     * @param buffered: Whether the writing should be buffered.
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void writeLines_append(Path path, List<String> lines, boolean buffered) throws IOException, FileNotFoundException
    {
    	File file = path.toFile();
    	File parent = file.getParentFile();
    	if (parent.exists())
    	{
    		if (!parent.isDirectory())
    			throw new IllegalArgumentException("Parent file needs to be directory!");
    	}
    	else
    	{
    		Files.createDirectories(parent.toPath());
    	}

    	Writer writer = new FileWriter(path.toFile(), true);
		if (buffered)
    		 writer = new BufferedWriter(writer);
    	for (String line : lines)
    	{
	    	writer.write(line + "\n");
    	}
    	writer.close();
    }

    /**
     * Same as riteKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, boolean overwrite, boolean buffered),
     * but with overwrite and buffered set to false.
     */
    public static void writeKeyValues(Path path, Map<? extends Object, ? extends Object> kvs) throws IOException, FileNotFoundException
    {
    	writeKeyValues(path, kvs, false, false);
    }

    /**
     * Same as riteKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, boolean overwrite, boolean buffered),
     * but with buffered set to false.
     */
    public static void writeKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, boolean overwrite) throws FileNotFoundException, IOException
    {
    	writeKeyValues(path, kvs, overwrite, false);
    }

    /**
     * Same as riteKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, String keyValueSeperator, boolean overwrite, boolean buffered),
     * but with keyValueSeperator set to '='
     */
    public static void writeKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, boolean overwrite, boolean buffered) throws FileNotFoundException, IOException
    {
    	writeKeyValues(path, kvs, "=", overwrite, buffered);
    }

    /**
     * Writes a map of key values to file, with each line containing a key.toString(),
     * followed by a '=' and it's corresponding value.toString().
     * @param path: Where the file is supposed to be written to.
     * @param kvs: The key value map supposed to be written.
     * @param keyValueSeperator: What should separate a key from a value in the file.
     * @param overwrite: Whether the method should overwrite an existing file at @param path.
     * @param buffered: Whether the data should be written with buffering.
     * @throws FileNotFoundException
     * @throws IOException: Anything else that can usually go wrong with opening & creating directories, files.
     */
    public static void writeKeyValues(Path path, Map<? extends Object, ? extends Object> kvs, String keyValueSeperator, boolean overwrite, boolean buffered) throws FileNotFoundException, IOException
    {
    	File file = path.toAbsolutePath().toFile();
    	File parent = file.getParentFile();

    	if (parent.exists())
    	{
    		if (!parent.isDirectory())
    			throw new IllegalArgumentException("Parent file needs to be directory!");
    	}
    	else
    	{
    		Files.createDirectories(parent.toPath());
    	}

    	if (file.exists())
    	{
    		if (!overwrite)
    			throw new IllegalArgumentException("Target file exists & overwrite is set to false!");
    	}
    	else
    		file.createNewFile();

    	Writer writer = new FileWriter(path.toFile());
		if (buffered)
    		 writer = new BufferedWriter(writer);
    	for (Object key : kvs.keySet())
    	{
	    	writer.write(key + keyValueSeperator + kvs.get(key) + "\n");
    	}
    	writer.close();
    }

    /**
     * Writes a map of key values to file, with each line containing a key.toString(),
     * followed by a '=' and it's corresponding value.toString().
     * @param path: Where the file is supposed to be written to.
     * @param kvs: The key value map supposed to be written.
     * @param buffered: Whether the method should buffer the write.
     * @throws FileNotFoundException
     * @throws IOException: Anything else that can usually go wrong with opening & creating directories, files.
     */
    public static void writeKeyValues_append(Path path, Map<? extends Object, ? extends Object> kvs, boolean buffered) throws FileNotFoundException, IOException
    {
    	File file = path.toFile();
    	File parent = file.getParentFile();
    	if (parent.exists())
    	{
    		if (!parent.isDirectory())
    			throw new IllegalArgumentException("Parent file needs to be directory!");
    	}
    	else
    	{
    		Files.createDirectories(parent.toPath());
    	}

    	Writer writer = new FileWriter(path.toFile(), true);
		if (buffered)
    		 writer = new BufferedWriter(writer);
    	for (Object key : kvs.keySet())
    	{
	    	writer.write(key + "=" + kvs.get(key) + "\n");
    	}
    	writer.close();
    }

    /**
     * Same as writeKeyValues_append(Path path, Map<? extends Object, ? extends Object> kvs, boolean buffered), but unbuffered.
     */
    public static void writeKeyValues_append(Path path, Map<? extends Object, ? extends Object> kvs) throws FileNotFoundException, IOException
    {
    	writeKeyValues_append(path, kvs, false);
    }

    public static Map<String, List<D2IntPoint>> findOccurences(Path path, String string)
    {
    	return findOccurences(path, string, false);
    }

    public static Map<String, List<D2IntPoint>> findOccurences(List<String> lines, String string)
    {
    	return findOccurences(lines, string, false);
    }

    public static Map<String, List<D2IntPoint>> findOccurences(Path path, String string, boolean regex)
    {
    	try {
			return findOccurences(Files.readAllLines(path), string, regex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new HashMap<>();
		}
    }

    public static Map<String, List<D2IntPoint>> findOccurences(List<String> lines, String string, boolean regex)
    {
    	Map<String, List<D2IntPoint>> positionMap = new HashMap<>();
		if (regex)
		{
			for (int i = 0; i < lines.size(); i++)
			{
				int start = 0;
				int index = -1;
				String line = lines.get(i);
				do
				{
					Pattern pattern = Pattern.compile(string);
					Matcher matcher = pattern.matcher(line.substring(start));
					if(matcher.find())
					{
						String val = matcher.group();
						index = matcher.start();
						List<D2IntPoint> points = positionMap.get(val);
						points = points != null ? points : new ArrayList<>();
						points.add(new D2IntPoint(i, index));
						positionMap.put(val, points);
						start = index + val.length();
					}
					index = -1;
				}
				while (index != -1);
			}
		}
		else
		{
			for (int i = 0; i < lines.size(); i++)
			{
				int index;
				int start = 0;
				String line = lines.get(i);
				while ((index = line.indexOf(string, start)) != -1)
				{
					List<D2IntPoint> points = positionMap.get(string);
					points = points != null ? points : new ArrayList<>();
					points.add(new D2IntPoint(i, index));
					positionMap.put(string, points);
					start = index + string.length();
				}
			}
		}
    	return positionMap;
    }

    public static void dividingLine(int symbols, int lineBreaksPre, int lineBreaksPost)
	{
		if (lineBreaksPre > 0)
		{
			for (int i = 0; i < lineBreaksPre; i++)
				System.out.println();
		}
		String s = "";
		for (int i = 0; i < symbols; i++)
		{
			s += "-";
		}
		sout(s);
		if (lineBreaksPost > 0)
		{
			for (int i = 0; i < lineBreaksPost; i++)
				System.out.println();
		}
	}

    public static void dividingLine()
	{
		dividingLine(0);
	}

	public static void dividingLine(int lineBreaks)
	{
		dividingLine(20, lineBreaks, lineBreaks);
	}

	public static void dividingLine(int lineBreaksPre, int lineBreaksPost)
	{
		dividingLine(20, lineBreaksPre, lineBreaksPost);
	}

	public static  void dividingLine(boolean lineBreaks)
	{
		dividingLine(lineBreaks ? 1 : 0);
	}

	public static Charset detectCharset_def_UTF8(Path path)
	{
		return detectCharset(path, StandardCharsets.UTF_8);
	}

	public static Charset detectCharset(Path path)
	{
		return detectCharset(path, Charset.defaultCharset());
	}

	public static Charset detectCharset(Path path, Charset defaultCharset)
	{
		String charsetName = detectCharsetName(path);
		if (charsetName != null)
		{
			if (Charset.isSupported(charsetName))
			{
				return Charset.forName(charsetName);
			}
		}
		return defaultCharset;
	}

	public static String detectCharsetName(Path path)
	{
		byte[] buf = new byte[4096];
		try (FileInputStream fis = new FileInputStream(path.toFile());)
		{

			// (1)
			UniversalDetector detector = new UniversalDetector(null);

			// (2)
			int nread;
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			  detector.handleData(buf, 0, nread);
			}
			// (3)
			detector.dataEnd();

			// (4)
			String encoding = detector.getDetectedCharset();
			if (encoding != null) {
			  return encoding;
			}

			// (5)
			detector.reset();
		}
		catch (FileNotFoundException e){}
		catch (IOException e){}
		return null;
	}
	
	/**
	 * Capitalizes the first character of the given String
	 * @param str the string to be capitalized
	 * @return the given String with the first character capitalized or the original string if empty, or null if null 
	 */
	public static String capitalizeFirst(String str)
	{
		if (str != null)
		{
			if (!str.isEmpty())
			{
				return str.substring(0, 1).toUpperCase() + str.substring(1, str.length());
			}
			else return str;
		}
		else return null;
	}
}
