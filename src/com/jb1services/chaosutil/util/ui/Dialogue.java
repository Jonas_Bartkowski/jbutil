package com.jb1services.chaosutil.util.ui;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class Dialogue implements Runnable
{
    private boolean repeatOnEnd;
    private Map<Integer, String> inputs = new HashMap<>();
    private Map<Integer, String[]> possibleInputs = new HashMap<>();

    protected String fill(String prompt, String[] inputs, String replOpen, String replClose)
    {
        for (int i = 0; i < inputs.length; i++)
        {
        	String repl = replOpen+i+replClose;
            prompt = prompt.replace(repl, inputs[i]);
        }
        return prompt;
    }

    protected String fill(String prompt, String[] possibleInputs)
    {
        return fill(prompt, possibleInputs, "[", "]");
    }

    protected String fill(String prompt)
    {
        return fill(prompt, getPossibleInputs(), "[", "]");
    }

    protected String fillNums(String prompt)
    {
        return fill(prompt, getPossibleInputs(), "", "");
    }

    protected String[] newPossibleInputs(String... inputs)
    {
        possibleInputs.put(possibleInputs.size(), inputs);
        return inputs;
    }

    protected void popPossibleInputs()
    {
    	possibleInputs.remove(possibleInputs.size()-1);
    }

    protected String firstPossibleInput()
    {
    	return getPossibleInputs()[0];
    }

    protected boolean is1stPossibleInput()
    {
    	return isNthPossibleInput(0);
    }

    protected String secondPossibleInput()
    {
    	return getPossibleInputs()[1];
    }

    protected boolean is2ndPossibleInput()
    {
    	return isNthPossibleInput(1);
    }

    protected String thirdPossibleInput()
    {
    	return getPossibleInputs()[2];
    }

    protected boolean is3rdPossibleInput()
    {
    	return isNthPossibleInput(2);
    }

    protected boolean is4thPossibleInput()
    {
    	return isNthPossibleInput(3);
    }

    protected boolean is5thPossibleInput()
    {
    	return isNthPossibleInput(4);
    }

    protected boolean isNthPossibleInput(int n)
    {
    	return getPossibleInputs()[n].equalsIgnoreCase(getInput());
    }

    protected Input newInput(String input)
    {
        inputs.put(inputs.size(), input);
        return new Input(input);
    }

    protected Input newInputNextLine()
    {
    	String input = IOUtil.nextLine();
        inputs.put(inputs.size(), input);
        return new Input(input);
    }

    protected Input newInputNextLine(String prompt)
    {
    	String input = IOUtil.nextLine();
        inputs.put(inputs.size(), IOUtil.nextLine(prompt));
        return new Input(input);
    }

    protected Input newInput(Object input)
    {
    	return newInput(input.toString());
    }

    protected Input newInput(Boolean input)
    {
    	return newInput(input.toString());
    }

    protected Input newConfirmation(String prompt)
    {
    	return newInput(IOUtil.confirm(prompt));
    }

    protected Input newInput(Number input)
    {
    	return newInput(input != null ? input.toString() : null);
    }

    protected void popInput()
    {
    	inputs.remove(inputs.size()-1);
    }

    protected String[] getPossibleInputs()
    {
        return possibleInputs.get(possibleInputs.size()-1);
    }

    protected Input getInputObj()
    {
    	return new Input(inputs.get(inputs.size()-1));
    }

    protected String getInput()
    {
    	return inputs.get(inputs.size()-1);
    }

    protected int getInputAsInteger()
    {
    	return Integer.valueOf(getInput());
    }

    protected boolean getInputAsBoolean()
    {
    	return Boolean.valueOf(getInput());
    }

    protected boolean nothingEntered()
    {
    	return getInput().trim().isEmpty();
    }

    protected boolean nullResult()
    {
    	return getInput() == null;
    }

    public String next(String prompt)
	{
		return IOUtil.next(prompt, "Invalid input. Try again!", getPossibleInputs());
	}

    @Override
    public void run()
    {
    	call();
    	reset();
    }

    public void run(String... args)
    {
    	call(args);
    	reset();
    }

    protected void repeatOnEnd()
    {
    	this.repeatOnEnd = true;
    }

    protected void dontRepeatOnEnd()
    {
    	this.repeatOnEnd = false;
    }

    protected abstract void call(String... args);

    private void reset()
    {
    	this.inputs = new HashMap<>();
    	this.possibleInputs = new HashMap<>();
    	if (repeatOnEnd)
    	{
    		this.repeatOnEnd = false;
    		run();
    	}
    }

    public boolean inputNotNull()
    {
    	return getInput() != null;
    }

    public boolean inputNotEmptyNotNull()
    {
    	return getInput() != null && !getInput().trim().isEmpty();
    }

    public class Input
    {
    	private String input;

    	private Input(String input)
		{
			super();
			this.input = input;
		}

		public Optional<Integer> getAsInteger()
        {
			Integer i = null;
        	try
        	{
        		i = Integer.valueOf(input);
        	}
        	catch (NumberFormatException e)
        	{

        	}
        	return Optional.ofNullable(i);
        }

        public Optional<Boolean> getAsBoolean()
        {
        	Boolean b = null;
        	if (input.equalsIgnoreCase("true"))
        	{
        		b = true;
        	}
        	else if (input.equalsIgnoreCase("false"))
        	{
        		b = false;
        	}
        	return Optional.ofNullable(b);
        }

        public String getAsString()
        {
        	return input;
        }

        public <T> T getFromMap(Map<String, ? extends T> map)
        {
        	return map.get(input);
        }

        public Optional<Duration> getAsMinutes()
        {
        	Duration d = null;
        	try
        	{
        		d = Duration.ofMinutes(Long.valueOf(input));
        	}
        	catch (NumberFormatException e)
        	{

        	}
        	catch (ArithmeticException e2)
        	{

        	}
        	return Optional.ofNullable(d);
        }
    }
}
