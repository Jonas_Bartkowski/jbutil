package com.jb1services.chaosutil.util.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;

public class ChaosLinkedList<E> extends LinkedList<E> implements ChaosList<E>
{
    private static final long serialVersionUID = 1L;

    public ChaosLinkedList()
    {

    }

    public ChaosLinkedList(Collection<? extends E>... collections)
    {
        this(null, collections);
    }

    public ChaosLinkedList(Comparator<? super E> sorter, Collection<? extends E>... collections)
    {
        this.addAll(collections);
        if (sorter != null)
            this.sort(sorter);
    }
}
