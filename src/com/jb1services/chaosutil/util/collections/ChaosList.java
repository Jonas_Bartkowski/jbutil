package com.jb1services.chaosutil.util.collections;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ChaosList<E> extends List<E>
{
	public static <T> ChaosList<T> a_of(Collection<? extends T>... cs)
    {
		return new ChaosArrayList<T>(cs);
    }

	public static <T> ChaosList<T> l_of(Collection<? extends T>... cs)
    {
		return new ChaosLinkedList<T>(cs);
    }

	public default Stream<E> filter_s(Predicate<E> pred)
    {
		return this.stream().filter(pred);
    }

    public default ChaosList<E> filter(Predicate<E> pred)
    {
    	if (this instanceof ChaosLinkedList)
    	{
    		return l_of(this.stream().filter(pred).collect(Collectors.toList()));
    	}
    	else
    		return a_of(this.stream().filter(pred).collect(Collectors.toList()));
    }

    public default E last()
    {
        if (this.size() > 0)
            return this.get(this.size()-1);
        else
            throw new IllegalStateException("Trying to return last element of size 0 list.");
    }

    public default Optional<E> findFirst(Predicate<E> pred)
    {
        return this.stream().filter(pred).findFirst();
    }

    public default Optional<E> findAny(Predicate<E> pred)
    {
        return this.stream().filter(pred).findAny();
    }

    public default Optional<E> findLast(Predicate<E> pred)
    {
        List<E> matches = this.stream().filter(pred).collect(Collectors.toList());
        if (matches.size() > 0)
            return Optional.of(matches.get(matches.size()-1));
        else
            return Optional.ofNullable(null);
    }

    public default void addAll(Collection<? extends E>... collections)
    {
        for (Collection<? extends E> collection : collections)
            this.addAll(collection);
    }

    public default ChaosList<E> combine(Collection<? extends E>... collections)
    {
        if (collections != null && collections.length > 0)
        {
	        ChaosList<E> toRet = new ChaosArrayList<>();
	        toRet.addAll(this);
	        toRet.addAll(collections);
	        return toRet;
        }
        return null;
    }
    
    /**
     * Removes and returns first element of list
     */
    public default E shift()
    {
    	E e = null;
    	if (this.size() > 0)
    	{
    		e = this.remove(0);
    	}
    	return e;
    }
}
