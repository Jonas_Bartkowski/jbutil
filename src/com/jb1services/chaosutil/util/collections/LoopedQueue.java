package com.jb1services.chaosutil.util.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;

public class LoopedQueue<E> extends LinkedList<E> implements Queue<E>, ChaosList<E>
{
	private static final long serialVersionUID = 1L;

	public LoopedQueue()
	{

	}

	public LoopedQueue(Collection<? extends E> collection)
	{
		this.addAll(collection);
	}

	public LoopedQueue(Collection<? extends E>... collections)
	{
		this(null, collections);
	}

	public LoopedQueue(Comparator<? super E> sorter, Collection<? extends E>... collections)
	{
		this.addAll(collections);
		if (sorter != null)
			this.sort(sorter);
	}

	public E poll()
	{
		E e = super.poll();
		super.add(e);
		return e;
	}

	public void rotateTo(Optional<E> eo)
	{
		rotateTo(eo, 0);
	}

	public void rotateTo(E e)
	{
		rotateTo(e, 0);
	}

	public void rotateTo(Optional<E> eo, int advance)
	{
		if (eo.isPresent())
		{
			E e = eo.get();
			E first = this.peek();
			if (first.equals(e))
			{
				for (int i = 0; i < advance; i++)
					poll();
				return;
			}

			this.poll();
			while (!this.peek().equals(e) && this.peek() != first)
			{
				this.poll();
			}

			if (this.peek().equals(e))
			{
				for (int i = 0; i < advance; i++)
					poll();
			}
			else throw new IllegalStateException("ClosedLoop doesn't contain rotation target");
		}
	}

	public void rotateTo(E e, int advance)
	{
		rotateTo(Optional.ofNullable(e), advance);
	}

	public void rotateTo(int i)
	{
		rotateTo(this.get(i));
	}

	public E getNext(E e)
	{
		return this.indexOf(e) == this.size()-1 ? this.get(0) : this.get(this.indexOf(e) + 1);
	}
}
