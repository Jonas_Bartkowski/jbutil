package com.jb1services.chaosutil.util.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class ChaosArrayList<E> extends ArrayList<E> implements ChaosList<E>
{
	private static final long serialVersionUID = 1L;

	public ChaosArrayList()
	{

	}

	public ChaosArrayList(Collection<? extends E>... collections)
	{
		this(null, collections);
	}

	public ChaosArrayList(Comparator<? super E> sorter, Collection<? extends E>... collections)
	{
		this.addAll(collections);
		if (sorter != null)
			this.sort(sorter);
	}
}
