package com.jb1services.chaosutil.util.helpers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

import com.jb1services.chaosutil.util.formating.FormatUtil;
import com.jb1services.chaosutil.util.ui.IOUtil;

public class ConfigDefinition implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_KEY_VALUE_SEPERATOR = "=";
	private static final String DEFAULT_END_OF_CONFIG_MARKER = "[CONFIG_END]";

	protected transient Path targetFile;
	protected Map<String, Pattern> attributeConstraints;
	protected Map<String, String> defaults;
	private String endOfConfigMarker = DEFAULT_END_OF_CONFIG_MARKER;
	protected String keyValueSeperator = DEFAULT_KEY_VALUE_SEPERATOR;

	private void init()
	{
		this.attributeConstraints = new HashMap<>();
		this.defaults = new HashMap<>();
	}
	
	public ConfigDefinition()
	{
		init();
	}

	public ConfigDefinition(Path targetFile)
	{
		this();
		this.targetFile = targetFile;
	}
	
	protected ConfigDefinition(Path targetFile, Map<String, String> defaults,  Map<String, Pattern> attributeConstraints)
	{
		this(targetFile, attributeConstraints);
		this.defaults = defaults;
	}
	
	protected ConfigDefinition(Path targetFile, Map<String, Pattern> attributeConstraints)
	{
		this();
		this.targetFile = targetFile;
		this.attributeConstraints = attributeConstraints;
	}
	
	protected ConfigDefinition(Path targetFile, Map<String, Pattern> attributeConstraints, String keyValueSeperator)
	{
		this(targetFile, attributeConstraints);
		this.keyValueSeperator = keyValueSeperator;
	}

	protected ConfigDefinition(Path targetFile, String keyValueSeperator, String[] attributeNames)
	{
		this(targetFile,
		        Arrays.asList(attributeNames).stream().collect(Collectors.toMap(at -> at, at -> Pattern.compile(".*"))),
		        keyValueSeperator);
	}
	
	public static ConfigDefinition make(Path targetFile, Map<String, String> defaults)
	{
		ConfigDefinition config = new ConfigDefinition();
		config.init();
		config.targetFile = targetFile;
		config.defaults = defaults;
		return config;
	}
	
	public Path getTargetPath()
	{
		return targetFile;
	}

	public void setTargetFile(Path targetPath)
	{
		this.targetFile = targetPath;
	}

	public void setTargetPath(Path targetPath)
	{
		this.targetFile = targetPath;
	}

	public ConfigDefinition(Map<String, Pattern> validValuesDefinition)
	{
		this.attributeConstraints = validValuesDefinition;
	}

	public ConfigDefinition(String[] keys, String... patterns) throws PatternSyntaxException
	{
		Map<String, Pattern> map = new HashMap<>();
		for (int i = 0; i < keys.length; i++)
		{
			map.put(keys[i], Pattern.compile(patterns[i]));
		}
		this.attributeConstraints = map;
	}

	public String getValueForAttribute(String attribute, Path targetConfig) throws IOException
	{
		Pattern regexForAttribute = attributeConstraints.get(attribute);
		List<String> lines = Files.readAllLines(targetConfig, Charset.defaultCharset());
		boolean endDetected = false;
		for (String line : lines)
		{
			if (line.contains(endOfConfigMarker))
			{
				endDetected = true;
				line = line.substring(0, line.indexOf(endOfConfigMarker));
			}

			String[] splits = line.split(keyValueSeperator);
			if (splits.length >= 2 && splits[0].equals(attribute))
			{
				List<String> splitsList = FormatUtil.toList(1, splits);
				String value = splitsList.stream().collect(Collectors.joining(keyValueSeperator));
				if (regexForAttribute != null)
				{
					Matcher matcher = regexForAttribute.matcher(value);
					if (matcher.find()) {
						return value;
					}
				}
				else
					return value;
			}

			if (endDetected) return null;
		}
		return defaults.get(attribute);
	}

	public String getValueForAttribute(String attribute) throws IOException
	{
		return getValueForAttribute(attribute, this.targetFile);
	}

	public Map<String, String> getAttributeValueMap() throws IOException
	{
		return getAttributeValueMap(this.targetFile);
	}

	public Map<String, String> getAttributeValueMap(Path targetConfig) throws IOException
	{
		Map<String, String> map = new HashMap<>();
		map.putAll(defaults);
		List<String> lines;
		boolean endDetected = false;
		lines = Files.readAllLines(targetConfig, Charset.defaultCharset());
		for (String line : lines)
		{
			if (line.contains(endOfConfigMarker))
			{
				endDetected = true;
				line = line.substring(0, line.indexOf(endOfConfigMarker));
			}

			String[] splits = line.split(keyValueSeperator);
			if (splits.length >= 2)
			{
				String attributeName = splits[0];
				Pattern valuePatt = attributeConstraints.get(attributeName);
				if (valuePatt != null)
				{
					List<String> splitsList = FormatUtil.toList(1, splits);
					String value = splitsList.stream().collect(Collectors.joining(keyValueSeperator));
					Matcher matcher = valuePatt.matcher(value);
					if (matcher.find())
					{
						map.put(attributeName, value);
					}
				} else
				{
					map.put(attributeName, splits[1]);
				}
			}
			if (endDetected) return map;
		}
		return map;
	}

	public String expandVariables(String input) throws IOException
	{
		Map<String, String> vals = this.getAttributeValueMap();
		for (String string : vals.keySet())
		{
			input = input.replace("$" + string, vals.get(string));
		}
		return input;
	}
	
	public void writeDefaultsToDisk() throws FileNotFoundException, IOException
	{
		Map<String, String> toWrite = new HashMap<>();
		toWrite.putAll(defaults);
		Map<String, String> readValues;
		try
		{
			readValues = getAttributeValueMap();
		} catch (IOException e)
		{
			readValues = new HashMap<>();
		}
		readValues.keySet().forEach(key -> toWrite.remove(key));
		IOUtil.writeKeyValues_append(targetFile, toWrite);
	}
	
	/**
	 * Same as updateFile(Map<String, String> values)
	 * only that every second string in the @param values array, 
	 * is a value (i+1) and the value before that is it's key (i).
	 * @param values The values the config is supposed to be updated with.
	 * @throws IOException
	 */
	public void updateFile(String... values) throws IOException
	{
		Map<String, String> valuesMap = new HashMap<>();
		for (int i = 0; i+1 < values.length; i+=2)
		{
			valuesMap.put(values[i], values[i+1]);
		}
		updateFile(valuesMap);
	}
	
	/**
	 * Updates the config file with the given @param values in the following way:
	 * 1) Put all default key-values into a map.
	 * 2) Load and put all key-values in the file into that same map.
	 * 3) Put the given @param key-values into that same map.
	 * 4) Overwrite that that map to targetFile. 
	 * 
	 * That means the priority in a conflict is:
	 * value given in @param values > value from file > value from defaults.
	 * @param values The values the file is supposed to be updated with.
	 * @throws IOException
	 */
	public void updateFile(Map<String, String> values) throws IOException
	{
		Map<String, String> base = new HashMap<>();
		base.putAll(values);
		Map<String, String> toWrite = new HashMap<>();
		try
		{
			toWrite = getAttributeValueMap();
		}
		catch (IOException e)
		{
			toWrite.putAll(defaults);
		}
		toWrite.putAll(base);
		IOUtil.writeKeyValues(targetFile, toWrite, true);
	}

	private void writeObject(ObjectOutputStream aOutputStream) throws IOException
	{
		aOutputStream.defaultWriteObject();
		aOutputStream.writeUTF(targetFile.toAbsolutePath().toString());
	}

	private void readObject(ObjectInputStream aInputStream) throws IOException, ClassNotFoundException
	{
		aInputStream.defaultReadObject();
		this.targetFile = Paths.get(aInputStream.readUTF());
	}
}
