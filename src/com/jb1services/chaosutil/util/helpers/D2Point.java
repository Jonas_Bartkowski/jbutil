package com.jb1services.chaosutil.util.helpers;

public class D2Point<E> 
{
	private E x;
	private E y;
	
	public D2Point(E x, E y) 
	{
		super();
		this.x = x;
		this.y = y;
	}

	public E getX() 
	{
		return x;
	}

	public void setX(E x) 
	{
		this.x = x;
	}

	public E getY() 
	{
		return y;
	}

	public void setY(E y) 
	{
		this.y = y;
	}
}
