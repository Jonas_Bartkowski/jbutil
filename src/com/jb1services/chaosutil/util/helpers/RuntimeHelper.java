package com.jb1services.chaosutil.util.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class RuntimeHelper
{
	public static void runCommandAndPrintOutput(String commandLineToken, String[] envp, Path targetDirectory)
	{
		runCommandAndPrintOutput(new String[] {commandLineToken}, envp, targetDirectory);
	}

	public static void runCommandAndPrintOutput(String commandLineToken, Path targetDirectory)
	{
		runCommandAndPrintOutput(new String[] {commandLineToken}, null, targetDirectory);
	}

	public static void runCommandAndPrintOutput(String[] commandLineTokens, String[] envp, Path targetDirectory)
	{
		Runtime rt = Runtime.getRuntime();
		try
		{
		      String line;
		      Process p = rt.exec("gradle build", null, targetDirectory.toFile());
		      BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		      while ((line = input.readLine()) != null)
		      {
		    	  System.out.println(line);
		      }
		      input.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error when finding target directory!");
		}
	}

	public static List<String> runCommandAndReturnOutput(String commandLineToken, String[] envp, Path targetDirectory)
	{
		return runCommandAndReturnOutput(new String[] {commandLineToken}, envp, targetDirectory);
	}

	public static List<String> runCommandAndReturnOutput(String commandLineToken, Path targetDirectory)
	{
		return runCommandAndReturnOutput(new String[] {commandLineToken}, null, targetDirectory);
	}

	public static List<String> runCommandAndReturnOutput(String[] commandLineTokens, String[] envp, Path targetDirectory)
	{
		Runtime rt = Runtime.getRuntime();
		List<String> lines = new ArrayList<>();
		try
		{
		      Process p = rt.exec("gradle build", null, targetDirectory.toFile());
		      BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		      String line;
		      while ((line = input.readLine()) != null)
		      {
		    	  lines.add(line);
		      }
		      input.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error when finding target directory!");
		}
		return lines;
	}
}
