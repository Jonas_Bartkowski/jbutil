package com.jb1services.chaosutil.util.formating;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.Range;
import org.joda.time.LocalDate;

import com.jb1services.chaosutil.util.collections.ChaosArrayList;
import com.jb1services.chaosutil.util.collections.ChaosList;

public class FormatUtil
{
	public static String formatLongTime(long duration)
	{
		long millis = duration;
		long seconds = millis / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		long weeks = days / 7;
		long months = weeks / 4;
		long years = months / 12;

		return (years != 0 ? years + " years " : "") + (months != 0 ? (months - 12 * years) + " months " : "")
				+ (weeks != 0 ? (weeks - 4 * months) + " weeks " : "")
				+ (days != 0 ? (days - 7 * weeks) + " days " : "") + (hours != 0 ? (hours - 24 * days) + " hours " : "")
				+ (minutes != 0 ? (minutes - 60 * hours) + " minutes " : "")
				+ (seconds != 0 ? (seconds - 60 * minutes) + " seconds " : "") + (millis + " milliseconds.");
	}

	public static String formatLongDays(long duration)
	{
		return (duration / (1000 * 60 * 60 * 24)) + " days";
	}

	public static long getDays(long duration)
	{
		return duration / (1000 * 60 * 60 * 24);
	}

	public String dateToString(Date date)
	{
		return new SimpleDateFormat("dd.mm.yyy").format(date);
	}

	public Date stringToDate(String dateString)
	{
		DateFormat format = new SimpleDateFormat("dd.mm.yyy");
		try
		{
			return format.parse(dateString);
		} catch (ParseException e)
		{
			return null;
		}
	}

	public static String formatBiggestTimeUnit(long duration)
	{
		long millis = duration;
		long seconds = millis / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		long weeks = days / 7;
		long months = weeks / 4;
		long years = months / 12;

		return (years != 0 ? years + " years"
				: months != 0 ? months + " months"
						: weeks != 0 ? weeks + " weeks"
								: days != 0 ? days + " days"
										: hours != 0 ? hours + " hours"
												: minutes != 0 ? minutes + " minutes"
														: seconds != 0 ? seconds + " seconds"
																: millis + " milliseconds");
	}

	public static Range<Integer> stringToIntRange(String input)
	{
		return stringToIntRange(input, "-");
	}

	public static Range<Integer> stringToIntRange(String input, String sep)
	{
		if (input != null && input.contains(sep))
		{
			String[] splits = input.split(sep);
			if (splits.length == 2)
			{
				try
				{
					int a = Integer.valueOf(splits[0]);
					int b = Integer.valueOf(splits[1]);
					if (b > a)
						return Range.between(a, b);
				} catch (NumberFormatException e)
				{
				}
			}
		}
		return null;
	}

	public static Range<Double> stringToDoubleRange(String input)
	{
		return stringToDoubleRange(input, "-");
	}

	public static Range<Double> stringToDoubleRange(String input, String sep)
	{
		if (input != null && input.contains(sep))
		{
			String[] splits = input.split(sep);
			if (splits.length == 2)
			{
				try
				{
					double a = Double.valueOf(splits[0]);
					double b = Double.valueOf(splits[1]);
					if (b > a)
						return Range.between(a, b);
				} catch (NumberFormatException e)
				{
				}
			}
		}
		return null;
	}

	public static String formatInstant(Instant instant)
	{
		DateTimeFormatter formatter =
			    DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
			                     .withLocale(Locale.GERMANY)
			                     .withZone(ZoneId.systemDefault());
		return formatter.format(instant);
	}

	public static <T> List<T> toList(T... array)
	{
		return toList(0, array.length, array);
	}

	public static <T> List<T> toList(int start, T... array)
	{
		return toList(start, array.length, array);
	}

	public static <T> List<T> toList(int start, int end, T... array)
	{
		List<T> list = new ArrayList<>();
		for (int i = start; i < end; i++)
		{
			list.add(array[i]);
		}
		return list;
	}
	
	public static <T> ChaosList<T> toChaosList(T[] array)
	{
		return toChaosList(array, 0, array.length);
	}

	public static <T> ChaosList<T> toChaosList(T[] array, int start)
	{
		return toChaosList(array, start, array.length);
	}

	public static <T> ChaosList<T> toChaosList(T[] array, int start, int end)
	{
		ChaosList<T> list = new ChaosArrayList<>();
		for (int i = start; i < end; i++)
		{
			list.add(array[i]);
		}
		return list;
	}

	public static LocalDate sQLDateToLocalDate(Date date)
	{
		return new LocalDate(date.getTime());
	}

	public static Date jodaDateToSQLDate(LocalDate localDate)
	{
		return Date.from(Instant.ofEpochMilli(localDate.toDateTimeAtStartOfDay().getMillis()));
	}

	public static String jodaDateToSQLDateString(LocalDate localDate)
	{
		return localDate.getYear() + "-" + localDate.getMonthOfYear() + "-" + localDate.getDayOfMonth();
	}
	
	/**
	 * Converts the given val to a percentage with the given amount of decimal places. > 1.0 is converted to 1.0. < -1.0 is converted to -1.0.
	 * So 0.1 returns 10, -0.1 returns -10, 0.235 returns 23.5, etc.
	 * @param val
	 * @param dec
	 * @return
	 */
	public static double doubleFractionToPercentage(double val, int dec)
	{
		if (val > 1.0)
			val = 1.0;
		else if (val < -1.0)
			val = -1.0;
		
		double percent = val * 100;
		BigDecimal bd = new BigDecimal(percent).setScale(dec, RoundingMode.HALF_DOWN);
        return bd.doubleValue();
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
