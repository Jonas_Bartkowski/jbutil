package com.jb1services.chaosutil.structure.scraping;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jb1services.chaosutil.structure.interfaces.MapDataExtension;

public class Scrape extends MapDataExtension implements Serializable
{
	private static final long serialVersionUID = 1L;

	private long scrapeTime;

	public Scrape()
	{
		this.scrapeTime = System.currentTimeMillis();
	}

	public Scrape(long scrapeDate)
	{
		this.scrapeTime = scrapeDate;
	}

	public long getScrapeTime()
	{
		return this.scrapeTime;
	}

	public Date getScrapeDate()
	{
		return new Date(scrapeTime);
	}

	public String getFormattedScrapeDate()
	{
		return (new SimpleDateFormat("dd/MM/yyy hh:mm").format(getScrapeDate()));
	}
}
