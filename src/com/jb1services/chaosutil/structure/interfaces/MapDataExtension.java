package com.jb1services.chaosutil.structure.interfaces;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapDataExtension implements Serializable
{
	private static final long serialVersionUID = 1L;

	private Map<String, List<String>> data = new HashMap<>();

	protected List<String> getData(String key)
	{
		return data.get(key);
	}

	protected void setData(String key, List<String> data)
	{
		this.data.put(key, data);
	}

	protected void setData(String key, String... data)
	{
		setData(key, Arrays.asList(data));
	}

	protected String getSimpleData(String key)
	{
		return getData(key, 0);
	}

	protected String getData(String key, int index)
	{
		List<String> data = this.data.get(key);
		if (data != null && index < data.size())
		{
			return data.get(index);
		}
		return null;
	}

	protected String getKey(int keydex)
	{
		Iterator<String> it = this.data.keySet().iterator();
		int count = 0;
		while (it.hasNext())
		{
			if (count == keydex)
				return it.next();

			count++;
		}
		return null;
	}

	protected List<String> getData(int index)
	{
		if (this.size() < index)
		{
			int count = 0;
			for (String key : this.data.keySet())
			{
				if (count == index)
				{
					return this.data.get(key);
				}
			}
		}
		return null;
	}

	protected String getSimpleData(int entryIndex, int dataIndex)
	{
		List<String> data = getData(entryIndex);
		if (data != null && dataIndex < data.size())
		{
			return data.get(dataIndex);
		}
		return null;
	}

	protected String getSimpleData(int entryIndex)
	{
		return getSimpleData(entryIndex, 0);
	}

	public int size()
	{
		return this.data.size();
	}
}
