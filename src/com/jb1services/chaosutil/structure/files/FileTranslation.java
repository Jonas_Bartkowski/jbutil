package com.jb1services.chaosutil.structure.files;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileTranslation
{
	private Path originPath;
	private Path targetPath;

	public FileTranslation(String originPath, String targetPath)
	{
		this(Paths.get(originPath), Paths.get(targetPath));
	}

	public FileTranslation(Path originPath, Path targetPath)
	{
		super();
		this.originPath = originPath;
		this.targetPath = targetPath;
	}

	public Path getOriginPath()
	{
		return originPath;
	}

	public void setOriginPath(Path originPath)
	{
		this.originPath = originPath;
	}

	public Path getTargetPath()
	{
		return targetPath;
	}

	public void setTargetPath(Path targetPath)
	{
		this.targetPath = targetPath;
	}
}
