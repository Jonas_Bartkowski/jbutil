package com.jb1services.chaosutil.api.rest;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RestCalls
{
	private static final List<String> SUPPORTED_PROTOCOLLS = Arrays.asList("http://", "https://");

	public static JsonElement Get(String urlString)
	{
		final String test = urlString;
		if (!SUPPORTED_PROTOCOLLS.stream().anyMatch(prot -> test.startsWith(prot)))
			urlString = "http://" + urlString;

		Gson gson = new Gson();
		OkHttpClient client = new OkHttpClient();

		HttpUrl.Builder urlBuilder = HttpUrl.parse(urlString).newBuilder();
		// urlBuilder.addQueryParameter("id", "1");

		String url = urlBuilder.build().toString();

		Request request = new Request.Builder().url(url).build();
		Call call = client.newCall(request);
		try
		{
			Response response = call.execute();
			String responseJSON = response.body().string();
			JsonParser parser = new JsonParser();
			return parser.parse(responseJSON);
		} catch (SocketTimeoutException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static void putRequestWithHeaderAndBody(String url, String header, String jsonBody) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonBody);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .put(body) //PUT
                .addHeader("Authorization", header)
                .build();

        client.newCall(request);
    }

	public static Response postRequestWithHeaderAndBody(String url, String header, String jsonBody) throws IOException {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonBody);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                //.addHeader("Authorization", header)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }
}
